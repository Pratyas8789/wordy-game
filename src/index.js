
let func= require('./function');

if (!navigator.onLine) {
    alert("Sorry your internet is not working!! please try again. ")
} else{
    
    document.addEventListener("keydown", function name(event) {
        const key = event.key
        if (func.complete() == false) {
            if (key === 'Enter') {
                func.commit();
            } else if (key === 'Backspace') {
                func.backspace();
            } else if (func.isLetter(key)) {
                func.addletter(key.toUpperCase())
            }
        }
    });
}
